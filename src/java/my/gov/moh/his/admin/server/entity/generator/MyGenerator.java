package my.gov.moh.his.admin.server.entity.generator;

import my.gov.moh.his.admin.server.entity.Sequence;
import org.hibernate.HibernateException;
import org.hibernate.MappingException;
import org.hibernate.Transaction;
import org.hibernate.engine.spi.SharedSessionContractImplementor;
import org.hibernate.id.Configurable;
import org.hibernate.id.IdentifierGenerator;
import org.hibernate.query.Query;
import org.hibernate.service.ServiceRegistry;
import org.hibernate.type.Type;

import java.io.Serializable;
import java.util.Properties;

/**
 * Created by Thanis on 08-May-18.
 */
public class MyGenerator implements IdentifierGenerator, Configurable {

    private String prefix;
    private String seqType;

    @Override
    public Serializable generate(
            SharedSessionContractImplementor session, Object obj)
            throws HibernateException {

        Transaction transaction = session.beginTransaction();

        Long newLongSeq =  getSequence(session, seqType).getCurrSeq() + 1;
        String newSequence = prefix + newLongSeq;
        updateSequence(session, newSequence);

        transaction.commit();

        return newSequence;
    }

    @Override
    public void configure(Type type, Properties properties,
                          ServiceRegistry serviceRegistry) throws MappingException {
        prefix = properties.getProperty("prefix");
        seqType = properties.getProperty("seqType");
    }

    private void updateSequence(SharedSessionContractImplementor session, String newSequence) {
        Query query = session.createQuery("update Sequence set currSeq = :newLongSeq where seqType = :seqType");
        query.setParameter("currSeq", newSequence);
        query.setParameter("seqType", seqType);
        query.executeUpdate();
    }

    public Sequence getSequence(SharedSessionContractImplementor session, String seqType) {
        Query query = session.createQuery("SELECT s.currSeq FROM Sequence s WHERE s.seqType=:seqType");
        query.setParameter("seqType", seqType);
        return (Sequence) query.uniqueResult();
    }
}
