package my.gov.moh.his.admin.server.entity;

import java.io.Serializable;
import java.util.Date;

// a typical Java Bean which can be stored by many different ORM (object-relational mapping)
// systems, including Hibernate, Toplink, JDO, EJB3, etc.
public class SupplyItem implements Serializable {
    // this bean has no business logic.  It simply stores data in these instance variables.
    protected Long itemID;
    protected String SKU;
    protected String category;
    protected String itemName;
    protected String description;
    protected Double unitCost;
    protected String units;
    protected Boolean inStock;
    protected Date nextShipmentDate;

    // a zero-argument constructor is not required, but does enable certain convenience
    // features (see the docs for DMI)
    public SupplyItem() {
    }

    // SmartClient will call these getters when serializing a Java Bean to be transmitted to
    // client-side components.
    public Long getItemID() {
        return itemID;
    }

    // when receiving data from client-side SmartClient components, SmartClient will call these
    // setters to modify properties.  The setters are found via the Java Beans naming
    // convention, for example, a DataSource field named "category" will be applied via a
    // setter called setCategory().
    public void setItemID(Long id) {
        itemID = id;
    }

    public String getSKU() {
        return SKU;
    }

    public void setSKU(String sku) {
        SKU = sku;
    }

    public String getCategory() {
        return category;
    }

    public void setCategory(String c) {
        category = c;
    }

    public String getItemName() {
        return itemName;
    }

    public void setItemName(String name) {
        itemName = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String d) {
        description = d;
    }

    public Double getUnitCost() {
        return unitCost;
    }

    public void setUnitCost(Double cost) {
        unitCost = cost;
    }

    public String getUnits() {
        return units;
    }

    public void setUnits(String newUnits) {
        units = newUnits;
    }

    public Boolean getInStock() {
        return inStock;
    }

    public void setInStock(Boolean val) {
        inStock = val;
    }

    public Date getNextShipment() {
        return nextShipmentDate;
    }

    public void setNextShipment(Date date) {
        nextShipmentDate = date;
    }
}
