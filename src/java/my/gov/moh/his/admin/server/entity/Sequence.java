package my.gov.moh.his.admin.server.entity;

import java.io.Serializable;

/**
 * Created by Thanis on 17-May-18.
 */
public class Sequence implements Serializable{
    protected long seqId;
    protected String seqName;
    protected String seqType;
    protected Long currSeq;
    protected String prefix;

    public long getSeqId() {
        return seqId;
    }

    public void setSeqId(long seqId) {
        this.seqId = seqId;
    }

    public String getSeqName() {
        return seqName;
    }

    public void setSeqName(String seqName) {
        this.seqName = seqName;
    }

    public String getSeqType() {
        return seqType;
    }

    public void setSeqType(String seqType) {
        this.seqType = seqType;
    }

    public Long getCurrSeq() {
        return currSeq;
    }

    public void setCurrSeq(Long currSeq) {
        this.currSeq = currSeq;
    }

    public String getPrefix() {
        return prefix;
    }

    public void setPrefix(String prefix) {
        this.prefix = prefix;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Sequence)) return false;

        Sequence sequence = (Sequence) o;

        if (getSeqId() != sequence.getSeqId()) return false;
        if (!getSeqName().equals(sequence.getSeqName())) return false;
        if (!getSeqType().equals(sequence.getSeqType())) return false;
        if (!getCurrSeq().equals(sequence.getCurrSeq())) return false;
        return getPrefix().equals(sequence.getPrefix());
    }

    @Override
    public int hashCode() {
        int result = (int) (getSeqId() ^ (getSeqId() >>> 32));
        result = 31 * result + getSeqName().hashCode();
        result = 31 * result + getSeqType().hashCode();
        result = 31 * result + getCurrSeq().hashCode();
        result = 31 * result + getPrefix().hashCode();
        return result;
    }

    @Override
    public String toString() {
        return "Sequence{" +
                "seqId=" + seqId +
                ", seqName='" + seqName + '\'' +
                ", seqType='" + seqType + '\'' +
                ", currSeq=" + currSeq +
                ", prefix='" + prefix + '\'' +
                '}';
    }
}
